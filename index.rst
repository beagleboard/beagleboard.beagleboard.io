BeagleBoard.org Community Project Repositories
##############################################

This repository is the parent for projects seeking to create binary package feeds for individual projects.

Explore registered feeds at https://beagleboard.beagleboard.io.

.. note::

    BeagleBoard.org documentation project moved to https://git.beagleboard.org/docs/docs.beagleboard.io. 

Registered Project Repositories
===============================

..
   I don't know how to automatically generate this list, so, for now, send a merge request adding a new
   line to register your project repository.

* `freebasic <https://beagleboard.beagleboard.io/ci-freebasic>`_
* `librobotcontrol <https://beagleboard.beagleboard.io/librobotcontrol>`_
* `xenomai 3.0.x <https://beagleboard.beagleboard.io/ci-xenomai-v3.0.x>`_
* `xenomai 3.1.x <https://beagleboard.beagleboard.io/ci-xenomai-v3.1.x>`_
* `xenomai 3.2.x <https://beagleboard.beagleboard.io/ci-xenomai-v3.2.x>`_
* `u-boot <https://beagleboard.beagleboard.io/ci-u-boot/>`_
* `openocd <https://beagleboard.beagleboard.io/ci-openocd/>`_
* `mesa-sgx-23.3 <https://beagleboard.beagleboard.io/ci-mesa-sgx-23.3/>`_

Using Registered Project Repositories
=====================================

.. todo::

   I'm just setting up the registry home page. I don't yet know how to utilize these as package feeds.

Building Your Own Project Repository
====================================

.. todo::

   I'm just figuring this out too.

License
=======

This work is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_. Attribute original work to BeagleBoard.org Foundation.

.. image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
    :width: 88px
    :height: 31px
    :align: left
    :target: http://creativecommons.org/licenses/by-sa/4.0/

